import datetime
import os
import subprocess

import pandas as pd
import requests as re
from sqlalchemy import create_engine

import mazdap as mz

conn = mz.ObieeConnector()

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

rgtm_2_customer = pd.read_sql(
    """\
SELECT
	kr.*,
	SUBSTR(ZIP, 0, 5) AS ZIP_CODE,
	mdl_msrp.AVG_MSRP_AMT,
	CASE
		WHEN kr.EST_IN_MARKET_PROB >= 0.75 THEN 1
		ELSE 0
	END AS IN_MARKET_FLAG,
	cdcllf.MATURITY_DATE,
	cdcllf.SALE_DATE,
	cdcllf.SALES_CHANNEL,
	cdcllf.EARLY_TERM_DEALER_PURCHASE,
	CASE
		WHEN cdcllf.MATURITY_DATE BETWEEN DATE \'{quarter_start}\' AND DATE \'{quarter_end}\' THEN 1
		ELSE 0
	END AS LEASE_MATURE_NXT_QUARTER
FROM
	RPR_STG.KHN_RGTM2 kr
LEFT JOIN (
	SELECT
		REGEXP_REPLACE(MODEL_DESC, '_', '') AS MDL_CD,
		AVG(MSRP_AMT) AS AVG_MSRP_AMT
	FROM
		EDW_RPT.WC_VEHICLE_D wvd
	WHERE
		MSRP_AMT IS NOT NULL
	GROUP BY
		MODEL_DESC) mdl_msrp ON
	kr.MDL_CD = mdl_msrp.MDL_CD
LEFT JOIN C00_DLY_CHASE_LATEST_LEASE_FW cdcllf ON
	kr.VIN = cdcllf.VIN
	AND kr.OWNERSHIP_DATE = cdcllf.CONTRACT_DATE""".strip().format(
        quarter_start="2021-07-01", quarter_end="2021-09-30"
    ),
    engine,
)

rgtm_2_customer.to_csv("cust_data/fact_rgtm_customer.csv", index=False)

zip_mapping_master = pd.read_sql(
    """\
SELECT DISTINCT
	bdm.ZIP1_CD,
	bdm.MDA_CD,
	cddif.MKT_NM,
	cddif.DISTRICT,
	bdm.SOA_CD,
	UPPER(bdm.SOA_NM) AS SOA_NM,
	bdm.RGN_CD,
	bdm.ST_CD
FROM
	EDW_STG.BTC02010_DEALER_MASTER bdm
LEFT JOIN RPR_STG.C00_DLY_DEALER_INFO_FW cddif ON
	bdm.DLR_CD = cddif.DLR_CD
WHERE
	bdm.STATUS_CD = 'A'
	AND bdm.RGN_CD IN ('PA', 'NE', 'GU', 'MW')
	""".strip(),
    engine,
)

zip_mapping_master.to_csv("cust_data/dim_zip_mapping.csv", index=False)

#### SYNC ONEDRIVE ####
sync_cmd = "rclone sync -v cust_data sharepoint:wphyo/rgtm_customer"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####
