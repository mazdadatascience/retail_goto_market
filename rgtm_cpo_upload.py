import datetime
import os
import subprocess

import pandas as pd
import requests as re
from sqlalchemy import create_engine

import mazdap as mz

import psycopg2

conn = mz.ObieeConnector()

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

host = "mazda-datamart.cfisikaor4fw.us-west-2.redshift.amazonaws.com"
port = 5439
dbname = "datamart"
user = "hliu1"
password = "Hl@Mazda2018"


## get cpo marketing metrics
def etl_cpo_website_metrics():
    with psycopg2.connect(
        f"host='{host}' port={port} dbname='{dbname}' user={user} password={password}"
    ) as conn:
        sql1 = "SELECT * FROM mnaoprod.ds005_01_leads_elms_prod where calendar_date >= '2019-01-01' and vehicle_status = 'cpo' and lead_type in ('MUSA Leads', 'DW Leads', 'DR Leads')"
        leads_df = pd.read_sql_query(sql1, conn)
        sql2 = "SELECT * FROM mnaoprod.ds012_03_shiftdigital_dealervdpviews_prod where calendar_date >= '2020-01-01' and status='CPO'"
        cpo_vdp_df = pd.read_sql_query(sql2, conn)

    cal_master = pd.read_sql(
        "SELECT cal_date as calendar_date, sales_yr_mo_id FROM RPR_STG.KHN_CALENDAR_MASTER",
        engine,
    )

    leads_df["calendar_date"] = pd.to_datetime(leads_df["calendar_date"])
    cpo_vdp_df["calendar_date"] = pd.to_datetime(cpo_vdp_df["calendar_date"])

    leads_df = leads_df.merge(cal_master, how="left")
    leads_df = (
        leads_df.groupby(["sales_yr_mo_id", "dealer_id", "model"])["householdid"]
        .nunique()
        .reset_index(name="cpo_leads")
    )

    cpo_vdp_df = cpo_vdp_df.merge(cal_master, how="left")

    cpo_vdp_df = (
        cpo_vdp_df.groupby(["sales_yr_mo_id", "dealer_id", "model"])["vdp_views"]
        .sum()
        .reset_index(name="vdp_views")
    )

    leads_df.to_csv("data/leads_cpo.csv", index=False)
    cpo_vdp_df.to_csv('data/cpo_vdp.csv', index=False)

def etl_cpo_sls():
    cpo_sls = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Vehicle CPO Sales - By Dealer (Monthly)")
    cpo_sls.to_csv("data/cpo_sls.csv", index=False)

def etl_rgtm_cpo_master():
    rgtm_query = """
SELECT
	"Dealer ID",
	'' AS "Dealer",
	'' AS "Market",
	'' AS "Region",
	"Year",
	"Month",
	'' AS "Creative ID",
	'' AS "Creative Rotation",
	"Language",
	"Model",
	"Media Channel",
	"Media Tactic",
	"Spend",
	"Reach",
	"Impressions",
	"Clicks",
	"Video Starts",
	"Video 50%",
	"Video Completes",
	"Audio Starts",
	"Audio 50%",
	"Audio Completes",
	"Engagement",
	"TRPs",
	"Page Views",
	CPC,
	CPM,
	CTR,
	VCR
FROM
	AH_GEOMETRY_MASTER_CPO
""".strip()
    rgtm_master = pd.read_sql(rgtm_query, engine)
    rgtm_master.to_csv("data/rgtm_cpo_master.txt", index=False, sep="|")

#### MAIN ####
etl_cpo_website_metrics()
etl_cpo_sls()
etl_rgtm_cpo_master()
#### MAIN ####


#### SYNC ONEDRIVE ####
sync_cmd = "rclone sync -v data sharepoint:wphyo/rgtm"
with open("logging_cpo.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####

conn.logoff()
