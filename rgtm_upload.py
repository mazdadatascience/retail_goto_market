import datetime
import os
import subprocess

import pandas as pd
import requests as re
from sqlalchemy import create_engine

import mazdap as mz

conn = mz.ObieeConnector()

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

## get the necessary tables
def etl_date_master():
    today = datetime.date.today() - pd.DateOffset(30)

    date_master = pd.DataFrame(
        {"sls_date": pd.date_range(start="01/01/2018", end=today, freq="MS")}
    )
    cal_master = pd.read_sql(
        "SELECT cal_date as sls_date, sales_mo_total_days, sales_mo_name_yr FROM RPR_STG.KHN_CALENDAR_MASTER",
        engine,
    )
    date_master = date_master.merge(cal_master, how="left")
    date_master.to_csv("data/date_master.csv", index=False)
    cal_master.to_csv("data/cal_master.csv", index=False)

## comment out ##
def etl_dlr_vol():
    dlr_volume = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dealer Finance/DH Dealer Volume"
    )
    dlr_volume.to_csv("data/dlr_volume.csv", index=False)


def etl_kg_geom_pivot():
    kg_geom_pivot = pd.read_sql("SELECT * FROM KG_GEOMETRY_PIVOT", engine)
    kg_geom_pivot.to_csv("data/kg_geom_pivot.csv", index=False)


def etl_rtl_sls():
    rtl_sls = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Vehicle Sales - By Dealer (Monthly)"
    )
    rtl_sls.to_csv("data/rtl_sls.csv", index=False)


def etl_dcs_share():
    dcs_share = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/DCS Share"
    )
    dcs_share.to_csv("data/dcs_share_master.csv", index=False)

    dcs_sales_mtd = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/DCS Sales MTD"
    )
    dcs_sales_mtd.to_csv("data/dcs_sales_master.csv", index=False)


def etl_inventory_dol():
    inventory = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Pipeline - Inventory Mix by Dealer"
    )
    dol_df = conn.get_csv(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/RGTM/Days on Lot - by Dealer (Monthly)"
    )
    inventory.to_csv("data/inventory.csv", index=False)
    dol_df.to_csv("data/dol_df.csv", index=False)


def etl_re_stores():
    re_stores = pd.read_sql("SELECT * FROM RPR_STG.TM_RE_STORES", engine)
    re_stores.to_csv("data/re_stores.csv", index=False)


def etl_fm_metrics():
    eng_query = """
SELECT
	SALES_MO_NAME_YR,
	MODEL,
	MDA_CODE,
	SUM(ENGAGED_VISITS) AS ENGAGED_VISITS
FROM
	RPR_STG.WP_DS007_01_OMNITURE_PROD kdop
LEFT JOIN RPR_STG.KG_DMA_MDA_MAPPING kdmm ON
	kdop.DMA_CODE = kdmm.DMA_CODE
LEFT JOIN RPR_STG.KHN_CALENDAR_MASTER kcal ON kdop.CALENDAR_DATE = kcal.CAL_DATE
WHERE kdop.CALENDAR_DATE >= DATE '2018-01-01' and kdop."LANGUAGE" = 'ALL'
GROUP BY (SALES_MO_NAME_YR, MODEL, MDA_CODE)""".strip()
    eng_visits = pd.read_sql(eng_query, engine)
    vdp_query = """
SELECT
	SALES_MO_NAME_YR,
	CAST(DEALER_ID AS VARCHAR(5)) AS DEALER_ID,
	MODEL,
	SUM(VDP_VIEWS) AS VDP_VIEWS 
FROM
	RPR_STG.WP_DS012_03_DEALERVDPVIEWS wdd
LEFT JOIN RPR_STG.KHN_CALENDAR_MASTER kcal ON wdd.CALENDAR_DATE = kcal.CAL_DATE
WHERE
	CALENDAR_DATE >= DATE '2018-01-01'
GROUP BY (SALES_MO_NAME_YR, DEALER_ID, MODEL)""".strip()
    vdp_views = pd.read_sql(vdp_query, engine)
    leads_query = """
SELECT
	SALES_MO_NAME_YR,
	DEALER_ID,
	MODEL,
	HOUSEHOLDID
FROM
	WP_DS005_01_LEADS_ELMS_PROD wdlep
LEFT JOIN RPR_STG.KHN_CALENDAR_MASTER kcm ON
	wdlep.CALENDAR_DATE = kcm.CAL_DATE
WHERE
	wdlep.CALENDAR_DATE >= DATE '2018-01-01'
	AND VEHICLE_STATUS = 'new'
	AND LEAD_TYPE IN ('MUSA Leads', 'DW Leads', 'DR Leads')""".strip()
    elms_leads = pd.read_sql(leads_query, engine)

    eng_visits.to_csv("data/eng_visits.csv", index=False)
    vdp_views.to_csv("data/vdp_views.csv", index=False)
    elms_leads.to_csv("data/elms_leads.csv", index=False)


def etl_rgtm_master():
    rgtm_query = """
SELECT
	"Dealer ID",
	'' AS "Dealer",
	'' AS "Market",
	'' AS "Region",
	"Year",
	"Month",
	'' AS "Creative ID",
	'' AS "Creative Rotation",
	"Language",
	"Model",
	"Media Channel",
	"Media Tactic",
	"Spend",
	"Reach",
	"Impressions",
	"Clicks",
	"Video Starts",
	"Video 50%",
	"Video Completes",
	"Audio Starts",
	"Audio 50%",
	"Audio Completes",
	"Engagement",
	"TRPs",
	"Page Views",
	CPC,
	CPM,
	CTR,
	VCR
FROM
	AH_GEOMETRY_MASTER
""".strip()
    rgtm_master = pd.read_sql(rgtm_query, engine)
    rgtm_master.to_csv("data/rgtm_master.txt", index=False, sep="|")


#### MAIN ####
etl_date_master()
etl_dlr_vol()
etl_kg_geom_pivot()
etl_rtl_sls()
etl_dcs_share()
etl_inventory_dol()
etl_re_stores()
etl_fm_metrics()
etl_rgtm_master()
#### MAIN ####

#### SYNC ONEDRIVE ####
sync_cmd = " rclone sync -v data sharepoint:wphyo/rgtm --ignore-size --ignore-checksum --update"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####

conn.logoff()
